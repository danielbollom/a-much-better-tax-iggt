var express = require('express');
var router = express.Router();
request = require('superagent');

var apiAccessToken = '';
// API url
var apiUrl = 'https://graph.microsoft.com/v1.0';
// API app client id
var apiClientId = '922bbbd4-271d-46d8-a2f2-bc2586016e53';
// API client secret
var apiClientSecret = 'k6WalQO*-P.D6Y38SDUMRVMPnTzySS+L';
// The tennant ID where the app is installed
var apiTennant = '4edfcdee-ac8c-4276-ab8b-30fa0d1e6bdd';
// The OneDrive user who owns the Excel file
var apiDriveUser = 'ca853896-f389-4c4a-9ce9-123f3d68f050';
// The ID of the Excel file
// Use Graph Explorer to get the ID: https://developer.microsoft.com/en-us/graph/graph-explorer
/*var apiDriveItem = {
  // Taxes
  'income-tax': '01FAQNDF2S5ICT6KMOLBH3WI7KBLOTUS5X',
  'igtt': '01FAQNDF7EFXMHCFCSDJGKDOD3V25D3RQD',
  'my-igtt': '01FAQNDFZQR3VYW4E7RNE34WPGEXSXZCLL',
};*/

var excelFileNames = {
  'income-tax': 'Budget Model - Income Tax MBT - Master.xlsx',
  igtt: 'Budget Model - IGTT - Master.xlsx',
  'my-igtt': 'My IGTT Numbers - Master.xlsx',
};
// The actual budget figures
// These should align with the outputs of each model depending on the type
// We assume that there are equal tax, expense and balance values

// Updated 16/10/18
var budget = {
  'tax-1': 473745,
  'tax-2': 503666,
  'tax-3': 525458,
  'tax-4': 553966,
  'tax-5': 2056835, // Total
  'expense-1': 493573,
  'expense-2': 509094,
  'expense-3': 524967,
  'expense-4': 548830,
  'expense-5': 2076464,
  'future-1': 5366, // Adjustments
  'future-2': 7662,
  'future-3': 10466,
  'future-4': 11483,
  'future-5': 34977,
  'balance-1': -14462, // = tax - expense + adjustments/future
  'balance-2': 2234,
  'balance-3': 10957,
  'balance-4': 16619,
  'balance-5': 15348,
  'gdppercent-1': 0,
  'gdppercent-2': 0,
  'gdppercent-3': 0,
  'gdppercent-4': 0,
  'gdppercent-5': 0,
  'gdp-1': 1902956,
  'gdp-2': 1992990,
  'gdp-3': 2083038,
  'gdp-4': 2173604,
  'gdp-5': 8152589,
  'variance-1': 0,
  'variance-2': 0,
  'variance-3': 0,
  'variance-4': 0,
  'variance-5': 0,
};

/**
 * Get an access token to the Microsoftgraph
 * @param  {Function} callback Callback function
 */
var getAccessToken = function(callback) {
  request
    .post('https://login.microsoftonline.com/' + apiTennant + '/oauth2/v2.0/token')
    .type('form')
    .send({
      client_id: apiClientId,
      scope: 'https://graph.microsoft.com/.default',
      client_secret: apiClientSecret,
      grant_type: 'client_credentials',
    })
    .end(function(err, response) {
      if (err) return callback(err);
      apiAccessToken = response.body.access_token;
      return callback(null, response.body);
    });
};

/**
 * Opens the drive item for reading and writing.
 * @param  {String}   modelName      Machine name of the budget test determines which spreadsheet to open
 * @param  {Boolean}  persistChanges Optional, set to TRUE to persist changes
 * @param  {Function} callback       Callback function
 */
var getSession = function(modelName, persistChanges, callback) {
  // Persist changes is optional
  if (arguments.length < 3) {
    callback = arguments[1];
    persistChanges = false;
  }
  // Get an access token to the Microsoftgraph API
  getAccessToken(function(err) {
    if (err) return callback(err);
    console.log(modelName);
    // Request a new session
    request
      .post(
        apiUrl +
          '/users/' +
          apiDriveUser +
          '/drive/root:/MBT/' +
          excelFileNames[modelName] +
          ':/workbook/createSession'
      )
      //Old post request using drive item ID
      //.post(apiUrl + '/users/' + apiDriveUser + '/drive/items/' + apiDriveItem[modelName] + '/workbook/createSession')
      .set('Authorization', 'Bearer ' + apiAccessToken)
      .send({
        persistChanges: persistChanges,
      })
      .end(function(err, response) {
        if (err) return callback(err);
        return callback(null, response.body.id);
      });
  });
};

/**
 * Close the session when finished.
 * @param  {String}   modelName Machine name of the budget test determines which spreadsheet to open
 * @param  {String}   sessionId The session used to track the temporary Excel file
 * @param  {Function} callback  Callback function
 */
var closeSession = function(modelName, sessionId, callback) {
  request
    .post(
      apiUrl +
        '/users/' +
        apiDriveUser +
        '/drive/root:/MBT/' +
        excelFileNames[modelName] +
        ':/workbook/closeSession'
    )
    //.post(apiUrl + '/users/' + apiDriveUser + '/drive/items/' + apiDriveItem[modelName] + '/workbook/closeSession')
    .set('Authorization', 'Bearer ' + apiAccessToken)
    .set('Workbook-Session-Id', sessionId)
    .end(function(err, response) {
      if (callback) {
        if (err) return callback(err);
        return callback(null, response.body);
      }
    });
};

/**
 * Get the specially named table 'data_input' to retrive input/output fields.
 * @param  {String}   modelName Machine name of the budget test determines which spreadsheet to open
 * @param  {String}   sessionId The session used to track the temporary Excel file
 * @param  {Function} callback  Callback function
 */
var getInputTable = function(modelName, sessionId, callback) {
  request
    .get(
      apiUrl +
        '/users/' +
        apiDriveUser +
        '/drive/root:/MBT/' +
        excelFileNames[modelName] +
        ":/workbook/tables('data_input')/range"
    )
    //.get(apiUrl + '/users/' + apiDriveUser + '/drive/items/' + apiDriveItem[modelName] + '/workbook/tables(\'data_input\')/range')
    .set('Authorization', 'Bearer ' + apiAccessToken)
    .set('Workbook-Session-Id', sessionId)
    .send({})
    .end(function(err, response) {
      if (err) return callback(err);

      return callback(null, response.body);
    });
};

/**
 * Update the data within the data_input table.
 * @param  {String}   modelName    Machine name of the budget test determines which spreadsheet to open
 * @param  {String}   sessionId    The session used to track the temporary Excel file
 * @param  {Array}    data         Data to save back to the Excel file
 * @param  {String}   tableAddress The cell range to update
 * @param  {Function} callback     Callback function
 */
var updateInputTable = function(modelName, sessionId, data, tableAddress, callback) {
  tableAddress = tableAddress.split('!');
  tableAddress[0] = tableAddress[0].replace(/\'/g, '');

  request
    .patch(
      apiUrl +
        '/users/' +
        apiDriveUser +
        '/drive/root:/MBT/' +
        excelFileNames[modelName] +
        ':/workbook/worksheets/' +
        tableAddress[0] +
        "/range(address='" +
        tableAddress[1] +
        "')"
    )
    //.patch(apiUrl + '/users/' + apiDriveUser + '/drive/items/' + apiDriveItem[modelName] + '/workbook/worksheets/' + tableAddress[0] + '/range(address=\'' + tableAddress[1] + '\')')
    .set('Workbook-Session-Id', sessionId)
    .set('Authorization', 'Bearer ' + apiAccessToken)
    .send({
      values: data,
    })
    .end(function(err, response) {
      if (err) return callback(err);

      return callback(null, response.body);
    });
};

/**
 * Retrive the readable data from a list of rows
 * @param  {Array} rows  Rows in the range
 */
var getReadRows = function(rows) {
  var output = {};
  rows.forEach(function(row) {
    if (row[0] && row[0].toLowerCase().indexOf('out-') === 0) {
      if (row[1] == undefined || row[1] == '#VALUE!') {
        throw new Error('Invalid input');
      }
      // Trim whitespace
      if (typeof row[1] === 'string') {
        row[1] = row[1].trim();
      }
      output[row[0].toLowerCase()] = row[1];
    }
  });
  return output;
};

/**
 * Rather than santise the input, which could lead to an inacurate calculation,
 * throw an error.
 * @param  {String} input User input
 */
var sanitiseInput = function(input) {
  if (input && input.match(/[^A-Za-z0-9 ,\.\-\+$%]/gi)) {
    throw new Error('Invalid input ' + input);
  }
};

/**
 * Retrieve the model type from the spreadsheet, can be either tax or expense
 * @param  {Array} rows Rows in the data table of the spreadsheet
 * @return {String}     The model type, tax or expense
 */
var getModelType = function(rows) {
  var output;
  // Look for a code named type and get the correspending value
  rows.forEach(function(row) {
    if (row[0] && row[0].toLowerCase() == 'type') {
      output = row[1].toLowerCase().trim();
    }
  });
  if (output) {
    return output;
  } else {
    // Throw an error if now type is specified
    throw new Error('Model does not contain a type.');
  }
};

/**
 * Store the user input to the database
 * @param  {Object}   req       The Express request object
 * @param  {String}   modelName The machine name of the model being processed
 * @param  {Object}   dataLog   All the input and output values for the current model
 * @param  {Function} callback  Callback function
 */
var updateUserData = function(req, modelName, dataLog, callback) {
  // Load the monogdb collection
  var data = req.db.get('data');
  // Add the current users email address
  var userData = {
    email: req.user.email,
  };
  // Add the model name as the user may submit many models and these are all
  // stored together in a single entry
  userData['data.' + modelName] = dataLog;
  // Update (or insert) the data and then return the updated data which includes
  // all models they may have submitted
  data
    .findOneAndUpdate(
      {
        email: req.user.email,
      },
      {
        $set: userData,
      },
      {
        // Update or insert
        upsert: true,
        // By setting to false we retrieve the newly updated record
        returnOriginal: false,
      }
    )
    .then(function(updatedData) {
      return callback(updatedData);
    });
};

/**
 * Format a number to contain a thousands comma and add brackets instead of negatives
 * @param  {Number/Object} number The number or object to format
 * @return {String}               The number is now formatted and converted to a string
 */
var formatNumber = function(number) {
  // If the given number is indeed a number
  if (typeof number === 'number') {
    // Replace the negative sign with brackets
    if (number < 0) {
      return '(' + (number * -1).toLocaleString() + ')';
      // Replace 0 with -
    } else if (parseInt(number) == 0) {
      return '-';
    }
    // Return the number with thousands seperator
    return number.toLocaleString();

    // Alternatively an object may be given, in which case each value will be formatted
    // and a new object returned
  } else if (typeof number === 'object') {
    var newObject = {};
    for (var key in number) {
      if (number.hasOwnProperty(key)) {
        newObject[key] = formatNumber(number[key]);
      }
    }
    return newObject;
  }
  return number;
};

/**
 * Format a number to contain a thousands comma
 * @param  {Number/Object} number The number or object to format
 * @return {String}               The number is now formatted and converted to a string
 */
var formatNumberComma = function(number) {
  // If the given number is indeed a number
  if (typeof number === 'number') {
    // Return the number with thousands seperator
    return number.toLocaleString();

    // Alternatively an object may be given, in which case each value will be formatted
    // and a new object returned
  } else if (typeof number === 'object') {
    var newObject = {};
    for (var key in number) {
      if (number.hasOwnProperty(key)) {
        newObject[key] = formatNumberComma(number[key]);
      }
    }
    return newObject;
  }
  return number;
};

/**
 * Round number to specific decimal points
 * @param  {Number} number The number to format
 * @param  {Number} points The number of decimal points
 * @return {Number}        A float is returned
 */
var roundNumber = function(number, points) {
  var points = Math.round(points);
  if (points > 0) {
    number = parseFloat(number).toFixed(points);
    // Note there are some issues in rounding either way: https://stackoverflow.com/questions/11832914/round-to-at-most-2-decimal-places-only-if-necessary
  }
  return number;
};

/**
 * Given all the data a user has entered, calculate their budget
 * @param  {Object} userData A users data as stored in the db
 * @return {Object}          A budget based on the users own modelling
 */
var calculateUserBudget = function(userData) {
  var data = userData.data;
  // Clone the budget to start our own based on our variances without interferring
  // with the global budget values
  var budgetVaried = {};
  for (var budgetItem in budget) {
    if (budget.hasOwnProperty(budgetItem)) {
      budgetVaried[budgetItem] = budget[budgetItem];
    }
  }

  // Loop over each of the models in the stored user data which could be a
  // combination of tax (income) or expense data
  for (var modelName in data) {
    if (data.hasOwnProperty(modelName) && Object.entries(data[modelName]).length !== 0) {
      // For each model update the actual budget with the users variances to
      // build the users own custom budget data
      for (var budgetVariedItem in budgetVaried) {
        if (budgetVaried.hasOwnProperty(budgetVariedItem)) {
          var modelTypeItems = budgetVariedItem.split('-');
          var modelType = modelTypeItems[0];
          var budgetKey = modelTypeItems[1];
          var dataValue = data[modelName].values['out-' + budgetKey];
          // Update the varied budget with the corresponding output value based
          // on the users data and if the current model is a tax or expense
          if (data[modelName].type == modelType && dataValue) {
            budgetVaried[budgetVariedItem] += dataValue;
          }
        }
      }
    }
  }

  // Now that each of the models have been processed, do another pass to calculate
  // the budget balance (the running total)
  for (var budgetVariedItem in budgetVaried) {
    if (budgetVaried.hasOwnProperty(budgetVariedItem)) {
      var modelTypeItems = budgetVariedItem.split('-');
      var modelType = modelTypeItems[0];
      var budgetKey = modelTypeItems[1];
      var budgetBalance = budgetVaried['balance-' + budgetKey];
      if (modelType == 'tax') {
        // Assume there is equal matching taxes and expenses
        // Calculate the balance by subtracting expenses from taxes (income),
        // and subtracting the 'Future Fund Earnings' static values
        budgetVaried['balance-' + budgetKey] =
          budgetVaried['tax-' + budgetKey] -
          budgetVaried['expense-' + budgetKey] +
          budgetVaried['future-' + budgetKey];
        // Calculate the GDP percentage
        budgetVaried['gdppercent-' + budgetKey] =
          (budgetVaried['balance-' + budgetKey] / budgetVaried['gdp-' + budgetKey]) * 100;
        // Calculate the variance to the original Commonwealth budget
        budgetVaried['variance-' + budgetKey] =
          budgetVaried['balance-' + budgetKey] - budget['balance-' + budgetKey];
        // Format each of the numbers so they match the rest of the formatting
        budgetVaried['tax-' + budgetKey] = formatNumber(budgetVaried['tax-' + budgetKey]);
        budgetVaried['expense-' + budgetKey] = formatNumber(budgetVaried['expense-' + budgetKey]);
        budgetVaried['future-' + budgetKey] = formatNumber(budgetVaried['future-' + budgetKey]);
        budgetVaried['balance-' + budgetKey] = formatNumber(budgetVaried['balance-' + budgetKey]);
        budgetVaried['gdppercent-' + budgetKey] = formatNumberComma(
          roundNumber(budgetVaried['gdppercent-' + budgetKey], 1)
        );
        budgetVaried['variance-' + budgetKey] = formatNumber(budgetVaried['variance-' + budgetKey]);
      }
    }
  }

  return budgetVaried;
};

/**
 * Route for doing the calculation
 */
router.post('/:modelName', function(req, res, next) {
  // The model type is passed in via the URL and determines which file should be used.
  var modelName = req.params.modelName;

  // Open a temporary version of the Excel file
  getSession(modelName, function(err, sessionId) {
    if (err) return next(err);

    // Get the data_input tables so that each of the read/write cells can be iterated over
    getInputTable(modelName, sessionId, function(err, inputTable) {
      if (err) return next(err);
      // Catch any errors thrown by invalid characters
      try {
        // Capture data input and output so it can be stored later
        var dataLog = {
          input: {},
          output: {},
          values: {},
          type: false,
        };
        // Iterate of each of the writable cells to insert the user input
        inputTable.values.forEach(function(row) {
          // Add user input
          if (row[0] && row[0].toLowerCase().indexOf('in-') === 0) {
            if (req.body[row[0].toLowerCase()]) {
              sanitiseInput(req.body[row[0].toLowerCase()]);
              row[1] = req.body[row[0].toLowerCase()];

              if (req.user) {
                dataLog.input[row[0].toLowerCase()] = row[1];
              }
            }
            // Set cells to 'null' for all other cells which ignore it from the update
          } else {
            row.forEach(function(cell, index) {
              row[index] = null;
            });
          }
        });

        // Update the table which will also return the updated calculated values
        updateInputTable(modelName, sessionId, inputTable.values, inputTable.address, function(
          err,
          response
        ) {
          if (err) return next(err);
          closeSession(modelName, sessionId);
          // Extract just the readable fields
          var displayOutput;
          try {
            displayOutput = getReadRows(response.text);
            valueOutput = getReadRows(response.values);
            dataLog.output = displayOutput;
            dataLog.values = valueOutput;
            dataLog.type = getModelType(response.text);
          } catch (error) {
            return next(error);
          }

          // If the user is logged in then store the results and calulate the running total
          if (req.user) {
            try {
              // Store the users inputs and outputs and retrieve all their other data
              updateUserData(req, modelName, dataLog, function(userData) {
                // Calculate the users budget based on the retrieved data
                var userBudget = calculateUserBudget(userData);
                // Render the results
                var userOutput = {
                  [modelName]: userData.data[modelName].output,
                  igtt: userData.data.igtt.output,
                  igttValues: userData.data.igtt.values,
                  modelValues: userData.data[modelName].values,
                };
                return res.render('results', {
                  template: 'results_' + modelName.replace(/\-/g, '_'),
                  output: userOutput,
                  budget: formatNumber(budget),
                  userData: userData,
                  layout: false,
                });
              });
            } catch (error) {
              return next(error);
            }

            // Return the output without the users budget as the user is not logged in
          } else {
            var nonUserOutput = {
              [modelName]: displayOutput,
            };
            return res.render('results', {
              template: 'results_' + modelName.replace(/\-/g, '_'),
              output: nonUserOutput,
              layout: false,
            });
          }
        });
      } catch (error) {
        return next(error);
      }
    });
  });
});

module.exports = router;
