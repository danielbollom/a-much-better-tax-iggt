var express = require('express');
var router = express.Router();

/**
 * Retrieve a users data based on their email address
 * @param  {Object}   req       Express request object
 * @param  {String}   modelName The name of the model to return
 * @param  {Function} callback  Callback function
 */
var getUserData = function(req, modelName, callback) {
  // Data is stored in the data collection
  var data = req.db.get('data');
  data.findOne({ email: req.user.email }).then(function(userData) {
    if (userData && userData.data) {
      return callback(userData.data);
    } else {
      return callback();
    }
  });
};

//Gets supporter details for petition
var getSupporters = function(req, res, callback) {
  var supportersList = [];
  var data = req.db.get('users');
  data.find({ supporter: true }).then(function(data) {
    data.sort(function(a, b) {
      return new Date(a.dateSigned) - new Date(b.dateSigned);
    });
    var counter = 1;
    data.forEach(function(user) {
      supportersList.unshift({
        title: user.title,
        firstName: user.firstName,
        lastName: user.lastName,
        postcode: user.postcode,
        occupation: user.occupation,
        counter: counter,
        dateSigned: user.dateSigned,
      });
      counter++;
    });
    return callback(supportersList);
  });
};

/**
 * Render and return a response based on if the user is logged in
 * @param  {Object} req        Express request object
 * @param  {Object} res        Express response object
 * @param  {String} modelName  The machine name of the model being rendered
 * @param  {String} ModelTitle The human readable title to display on the rednered page
 */
var renderUserData = function(req, res, modelName, ModelTitle, supporters) {
  // If the user is logged in, retrieve all their data based on their email
  // so that the forms can be prefilled with their last saved values
  if (req.user) {
    getUserData(req, modelName, function(userData) {
      return res.render(modelName, {
        title: ModelTitle,
        user: req.user,
        userData: userData,
        supporters: supporters,
        pageName: modelName,
      });
    });
    // The user is not logged in, so just render the page with the defaults
  } else {
    return res.render(modelName, {
      title: ModelTitle,
      supporters: supporters,
      pageName: modelName,
    });
  }
};

/**
 * Routes for a basic content pages
 */

router.get('/designing-an-igtt', function(req, res, next) {
  return renderUserData(req, res, 'designing-an-igtt', 'Designing an IGTT');
});

router.get('/new-supporters', function(req, res, next) {
  return getSupporters(req, res, function(supportersList) {
    renderUserData(req, res, 'new-supporters', 'New Supporters', supportersList);
  });
});

router.get('/', function(req, res, next) {
  res.redirect('/designing-an-igtt');
});

module.exports = router;
