const express = require('express');
const path = require('path');
const hbs = require('hbs');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var logger = require('morgan');
var favicon = require('serve-favicon');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcryptjs');
var session = require('express-session');
const config = require('platformsh-config').config();

const app = express();

var calculate = require('./routes/calculate');
var content = require('./routes/content');
var user = require('./routes/user');

// Handlebars partials are being used to display the budget results tables
// For readability, each model is its own partial, all stored in the
// views/models directory. All reusable layout components are stored
// in the views/partials directory.
hbs.registerPartials(__dirname + '/views/models');
hbs.registerPartials(__dirname + '/views/partials');
hbs.registerPartials(__dirname + '/views/results');

// Handlebars helper to select an option in a dropdown based on the given data
// If no value is passed in then no modification is made so the form field should
// fall back to its default.
hbs.registerHelper('select', function(selected, options) {
  var output = options.fn(this);
  if (!selected) return output;
  // Remove any existing selected attributes that may exist in the template markup
  output = output.replace(' selected="selected"', '');
  // Add a selected attribute based on a matching value attribute
  output = output.replace(new RegExp(' value="' + selected + '"'), '$& selected="selected"');

  return output;
});

// Handlebars helper to check an option in a list of checkboxes or radios based
// on the given data. If no value is passed in then no modification is made so
// the form field should fall back to its default.
hbs.registerHelper('check', function(checked, options) {
  var output = options.fn(this);
  if (!checked) return output;
  // Remove any existing checked attributes that may exist in the template markup
  output = output.replace(' checked="checked"', '');
  // Add a checked attribute based on a matching value attribute
  output = output.replace(new RegExp(' value="' + checked + '"'), '$& checked="checked"');

  console.log(output);
  return output;
});

hbs.registerHelper('math', function(lvalue, operator, rvalue, operatorTwo, extraValue, options) {
  extraValue = parseFloat(extraValue);
  lvalue = parseFloat(lvalue);
  rvalue = parseFloat(rvalue);

  console.log(lvalue, rvalue);

  var result = {
    '+': lvalue + rvalue,
    '-': lvalue - rvalue,
    '*': lvalue * rvalue,
    '/': lvalue / rvalue,
    '%': lvalue % rvalue,
  }[operator];

  var secondResult = {
    '+': result + extraValue,
    '-': result - extraValue,
    '*': result * extraValue,
    '/': result / extraValue,
    '%': result % extraValue,
  }[operatorTwo];
  if (isNaN(secondResult)) {
    return '';
  }
  return formatNumberComma(secondResult);
});

/**
 * Format a number to contain a thousands comma
 * @param  {Number/Object} number The number or object to format
 * @return {String}               The number is now formatted and converted to a string
 */
var formatNumberComma = function(number) {
  // If the given number is indeed a number
  if (typeof number === 'number') {
    // Return the number with thousands seperator
    return number.toLocaleString();

    // Alternatively an object may be given, in which case each value will be formatted
    // and a new object returned
  } else if (typeof number === 'object') {
    var newObject = {};
    for (var key in number) {
      if (number.hasOwnProperty(key)) {
        newObject[key] = formatNumberComma(number[key]);
      }
    }
    return newObject;
  }
  return number;
};

if (process.env.PLATFORM_PROJECT) {
  var credentials = config.credentials('mongodb');
  var mongoPath = config.formattedCredentials('mongodb', 'mongodb');
  var db = require('monk')(mongoPath);
  app.use(function(req, res, next) {
    req.db = db;
    next();
  });
} else {
  var db = require('monk')('localhost/ambt');
  app.use(function(req, res, next) {
    req.db = db;
    next();
  });
}

// Setup of Passport
passport.use(
  new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true,
    },
    function(req, username, password, done) {
      db.collection('users').findOne({ email: username }, function(err, user) {
        if (err) {
          return done(err);
        }
        if (!user) {
          return done(null, false, { message: 'Incorrect username.' });
        }
        if (!validPassword(password, user)) {
          return done(null, false, { message: 'Incorrect password.' });
        }
        if (!user.activated) {
          return done(null, false, {
            message: 'Account not activated.',
          });
        }
        return done(null, user, { message: 'Login Successful.' });
      });
    }
  )
);

hashPassword = function(newUser, callback) {
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(newUser.password, salt, function(err, hash) {
      newUser.password = hash;
      callback(newUser);
    });
  });
};

validPassword = function(password, user) {
  return bcrypt.compareSync(password, user.password);
};

passport.serializeUser(function(user, done) {
  done(null, user.email);
});

passport.deserializeUser(function(email, done) {
  db.collection('users').findOne({ email: email }, function(err, user) {
    done(err, user);
  });
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');
app.set('view options', { layout: 'layout' });

app.disable('x-powered-by');

app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ 
  secret: process.env.SESSION_SECRET || 'qfkxi7f13Jca6oB8wNvH',    
 resave: true,
  saveUninitialized: true 
}));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(passport.initialize());
app.use(passport.session());
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));

app.use('/user', user);
app.use('/calculate', calculate);
app.use('/', content);

app.get('/', (req, res) => {
  res.render('index');
});

if (process.env.PLATFORM_PROJECT) {
  var port = normalizePort(config.port);
} else {
  var port = normalizePort(process.env.PORT || '8080');
}

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = {};

  if (req.app.get('env') === 'development') {
    res.locals.error = err;
    console.error(err);
  }

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(port, () => {
  console.log('App is running → PORT ' + port);
});
