{{> nav}}
<div class="page-banner">
    <div class="page-banner-inner">
        <h1 class="page-title">Designing an IGTT</h1>
    </div>
</div>
<div class="wrapper container">
    <div class="wrapper-inner row">
        <div class="container-body col-sm-12">
            <div class="inner ">
                <p>In looking at the IGTT numbers in more detail we should consider:</p>
                <ul>
                    <li>What different options for an IGTT could raise in terms of revenue?</li>
                    <li>What changes to income tax would be feasible as a trade-off?</li>
                    <li>How you would fare as an individual. Would you be better off with the current income tax and no
                        IGTT, or would you be better off with lower income taxes and a smaller inheritance?</li>
                </ul>
                <p>Let's examine each of these in turn.</p>

                <br />

                <h3>Designing an IGTT</h3>
                <p>The key variables in designing an IGTT are:</p>
                <ul>
                    <li>The scope of the tax, and critically:</li>
                    <ul>
                        <li>The amount of the tax-free threshold, below which no IGTT is payable</li>
                        <li>Whether gifts to family members are subject to the tax</li>
                        <li>Whether charitable gifts are subject to the tax</li>
                    </ul>
                    <br />
                    <li>Whether the IGTT is a flat tax like the GST, or progressive like income tax</li>
                    <ul>
                        <li>A flat tax applies the same rate to all estates and gifts, regardless of size</li>
                        <li>A progressive tax applies higher rates to larger estates and gifts</li>
                    </ul>
                    <br />
                    <li>If progressive, how many steps, and what the rates are above each step</li>
                    <br />
                </ul>
                <p>The model is a relatively simple one, though it does allow you to explore a wide range of options. It
                    assumes that any IGTT will be implemented with appropriate thought and care, and that the legislated
                    provisions are administered effectively. Tax administration is never perfect, and the model makes
                    allowance for the uncertainty in the size of the tax base, and for the normal vicissitudes of tax
                    collection (i.e. avoidance, evasion and neglect.)</p>
                <p>This is an important consideration, especially if you wish to impose high rates of tax. Higher taxes
                    do lead to increased avoidance, especially by those with the means to do so. Not to acknowledge this
                    is folly. However it is true that most of the assets that make up household wealth are very well
                    known, and not easily hidden or converted. That by itself means that this is not a tax easily
                    avoided.</p>
                <p>While we know a great deal about the size and makeup of household wealth in Australia, the quality of
                    those estimates deteriorates as one moves up the wealth scale - this is simply a function of the
                    sampling techniques employed by the Australian Bureau of Statistics and the University of Melbourne
                    (who run the Household Income and Labour Dynamics in Australia - 'HILDA' - study). Experts argue but
                    my judgement - reinforced by the ABS's specific cautions - is that above $20 million the
                    <i>magnitude</i> and <i>distribution</i> of wealth becomes too hard to reliably estimate. This group
                    represents about 10,000 households, or the top one-tenth of the top one per cent. The two things I
                    am
                    reasonably confident of is that the estimates we do have for this group underestimate - and not
                    overestimate - their <i>aggregate</i> wealth, and that in the overall scheme of things taxing them
                    punitively - even confiscatorily - does not increase the tax take by very much. There are simply
                    too few of them to make much of a difference in this context.</p>
                <p>An Inter-Generational Transfer Tax is unworkable without a tax on gifts made during one's life-time
                    (which extends, of course, to death's door). The model asks you to consider whether to tax
                    charitable and non-charitable gifts, and to set a single threshold and rate. The available data on
                    charitable gifts is very good, and these estimates are considered reasonably robust - though they do
                    not take into account a behavioural response by high wealth individuals to the imposition of an
                    IGTT. Most likely charitable donations would go up. There is no good data on non-charitable gifts in
                    Australia. What we can be sure of, based on overseas experience, is that a tax on non-charitable
                    gifts above a set threshold quickly establishes that threshold as the cap for annual family gifts.
                    That element of the model has been built with that observation in mind.</p>
                <p>The results are therefore to be regarded as <i>indicative</i>, though if you really push the
                    boundaries I would caveat the results as <i>speculative at the margin</i>, and unlikely to be fully
                    realised.</p>
                <p>You can calculate your own IGTT using the inputs below. You can log-in and save your result here: <a
                        href="/user/login?ref=designing-an-igtt" class="login-link">Log-in</a></p>

                <p>For a flat rate IGTT select the tax-free threshold and the rate that applies after that.</p>
                <ul>
                    <li>Select “0” for the tax-free threshold if you do not wish to have a tax-free threshold.</li>
                    <li>It is advisable to have some tax-free threshold, at least to eliminate estates of such low value
                        the cost of administering the tax would exceed the revenue able to be collected.</li>
                </ul>
                <p>For a progressive IGTT select the tax-free threshold and the thresholds and rates that apply above.
                </p>
                <ul>
                    <li>You can select up to six thresholds and rates (with multiple thresholds and rates each threshold
                        and rate must be higher than the previous one).</li>
                </ul>
            </div>
            <br />
            {{> igtt }}
            <br />
            <div class="inner ">
                <br />
                <p>Your calculated IGTT is shown in the top row of the table.</p>
                <p>For comparison purposes the revenue raised under a flat rate IGTT of 10%, 20%, 30% and 40% (with a
                    tax-free threshold of $18,200, and taxing non-charitable gifts with the same threshold and rate) is
                    also shown.</p>
                <p>There are lots of ways this revenue could be used, but for now let us consider what changes to income
                    tax could be made if we used all of the revenue for that purpose.</p>
            </div>
            <br />
            <div class="inner ">
                <h3>Income Tax</h3>

                <p>Income tax is complex, so for now we will concentrate on the basic thresholds and rates. There is
                    also an option to index the income tax thresholds – which we should do - and you can have up to nine
                    different thresholds and rates if you so choose.</p>
                <p>The model is benchmarked against 2018-19 estimates for income tax revenue and is considered reliable
                    for most scenarios. As with the IGTT model the income tax model makes allowance for evasion and
                    avoidance, but if you push the boundaries the results will be less certain.</p>
                <p>Remember that reductions in rates and thresholds at the bottom end of the scales benefit all 13
                    million odd taxpayers and are therefore very expensive to implement. Reductions or increases at the
                    top end benefit or penalise far fewer taxpayers (about 400,000 paid the top marginal rate in
                    2016-17, and contributed nearly 30% of total income tax revenue.)</p>
            </div>
            <br />
            {{> income_tax}}
            <br />
            <div class="inner ">
                <br />
                <p>Compare your combined IGTT and Alternative Income Tax result with the current Income Tax line. Has
                    one fully offset the other?</p>
                <p>Do you need to raise more IGTT revenue to fund your proposed reductions to income tax?</p>
                <p>Or do you need to raise more income tax than your initial alternative income tax to avoid having a
                    higher IGTT, or the need to raise other taxes, reduce spending, or increase debt?</p>
                <p> Or – sensibly – would you raise more than you need so that you can pay down debt and reduce the
                    interest cost of servicing it?</p>

            </div>
            <div class="inner">
                <br />
                <h3>Would I be better off?</h3>
                <p>That depends upon quite a lot of things, but if you are younger, not an only child, your parent(s) in
                    good health and not particularly wealthy, then yes, you will almost certainly be better off.</p>
                <p>Why?</p>
                <p>The simple reason is that for most people an inheritance is received quite late in life – usually
                    when we are in our sixties, sometimes a bit earlier and sometimes later. It is a long time to wait.
                    Things happen. Parents can divorce. Or run into business problems. Or play the pokies too hard. They
                    can win Tattslotto too. Reductions to income tax start straight away, and continue through life.
                    Furthermore the reductions in income tax leave more of your money in your own pocket, which you can
                    use to reduce a mortgage, to add to superannuation, or to improve your standard of living.</p>
                <p>Reducing a debt or making an investment in superannuation is doubly powerful – not only is the tax
                    saving put to work, but the saving itself compounds over time. If you are young the benefits are
                    large and enduring.</p>
                <p>It follows too that if you are older, or your parents quite wealthy, you may well be better off
                    paying higher income tax and waiting for the inheritance to drop in your lap.</p>
                <p>You probably recognize that a dollar now is worth more to you than the prospect of a dollar some time
                    in the future. Economists deal with this by applying a ‘discount rate’ – effectively a reduction in
                    the value of a dollar the further away it is, and you can apply a discount rate if you wish. There
                    is endless argument about the appropriate discount rate for different circumstances, but my guidance
                    is:</p>
                <ul>
                    <li>The riskier your inheritance the higher your discount rate should be.</li>
                    <li>Start with a number close to your mortgage rate.</li>
                    <li>If you don’t have one of those, choose 6% if you envisage a relatively low risk future, and a
                        higher discount rate if you think your inheritance is at greater risk.</li>
                </ul>
                <p>Discount rates are frequently manipulated – or chosen with regard to their impact on results. Try not
                    to do this.</p>
                <p>However, over very long timeframes – 30 and 40 years – even modest discount rates greatly diminish
                    the value of future benefits – even of very large sums.</p>
                <p>There is a broader perspective too.</p>
                <p>If we judge the acceptability of proposals for reform through the narrow lens of what it means for us
                    alone, few reforms worth anything will ever see the light of day.</p>
                <p>Reforms which improve national productivity and therefore economic growth will help lift all boats,
                    including yours, and a smaller inheritance (especially if it is already quite large) is a price
                    worth paying.</p>
                <p>But you may be surprised that in this case, for many people, especially younger people, there is a
                    happy coincidence between the national interest and self-interest.</p>

            </div>
            <div class="inner">
                <br />
                <h3>How much better off? Or worse off?</h3>

                <p>To answer that question we need to look at specific proposals for both the Inter-Generational
                    Transfer Tax and the corresponding changes to the current income tax.</p>
                <p>We cannot be categoric in answering this – there are just too many variables involved in real life
                    when it comes to individual cases. But we can certainly make reasonable assumptions that apply to
                    most people, and consider the results carefully.</p>
                <p>You can test your own circumstances in light of your proposals for an IGTT and the corresponding
                    changes to income tax by stepping through the questions below.</p>
                <p>You should know your current income, and you will need to make assumptions about your future income.
                    If you are still studying or undergoing training you should attempt to estimate your starting wage
                    or salary once you begin your career proper. After that you need to think about how you think your
                    career will pan out. Do you see yourself staying in the one job in the one place for life, with
                    little opportunity for advancement? Or do you see yourself as someone who will climb the corporate
                    or bureaucratic ladder rapidly? Will you set up and grow your own business? There are five
                    alternative futures, based on the limited amount we know about average career progression in
                    Australia. These options are named for their growth profile, and all are for real income growth
                    (i.e. without inflation): Very Low, Low, Steady, High and Very High. If you see yourself progressing
                    steadily but not spectacularly - i.e. about the same as everyone else - choose 'Steady'. If a bit
                    less confident about your prospects, choose "Low", or a bit more confident, choose "High". And if
                    you see yourself being left behind, or at the other end of the spectrum bolting up the income ladder
                    choose "Very Low" or "Very High" as appropriate. In all cases the profiles assume a somewhat faster
                    advancement early in our working life, which moderates gradually over future decades.</p>
                <p>Most people do not know the detail of their parents assets and liabilities, but you can make an
                    estimate. The value of the family home, any other real estate, and unused superannuation will most
                    likely make up the bulk of their estate, and representative values for the main components are shown
                    below. You probably know how many siblings it will be shared between, and roughly how long you will
                    have to wait.</p>
                <p>It is assumed that the estate of the first member of a couple to die transfers completely to their
                    spouse without any taxation. The model assumes that it is only when the last member of a couple
                    passes away – usually the wife – that the estate passes to the next generation and is subject to the
                    IGTT.</p>
                <p>You can choose a value based on a representative ‘average estate’ for each wealth quintile for
                    households 85+, or select your own. As an aid to developing your own estimate of your parents’
                    likely future worth, the main categories of assets and liabilities of older households, and the mean
                    values for households 75+ are also shown below. All are in 2018-19 prices. </p>
                <p>You can apply a discount rate if you so choose. It is considered advisable if only to recognise that
                    an inheritance is never a sure thing, and the further away it is the less certain you can be about
                    it. But bear in mind also that the discount rate applies to the whole of the cashflows and the
                    benefits of tax savings in the future diminish too.</p>

                <br />

                <div class="container">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4>Representative Estate, Households 85+, by Mean Wealth Quintile (rounded to nearest $10K)
                            </h4>
                            <table class="table">
                                <tr>
                                    <th class="text-left">Mean Wealth, Lowest Quintile 85+</th>
                                    <td class="text-right">$50,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Mean Wealth, Second Lowest Quintile 85+</th>
                                    <td class="text-right">$480,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Mean Wealth, Middle Quintile 85+</th>
                                    <td class="text-right">$910,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Mean Wealth, Second Highest Quintile 85+</th>
                                    <td class="text-right">$1,590,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Mean Wealth, Highest Quintile 85+</th>
                                    <td class="text-right">$3,900,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Mean Wealth, all Households 85+</th>
                                    <td class="text-right">$1,450,000</td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-6">
                            <h4>Own Estimate Checklist, Mean Household Assets and Liabilities, Households 75+</h4>
                            <table class="table">
                                <tr>
                                    <th class="text-left">Family Home</th>
                                    <td class="text-right">$590,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Other Real Estate</th>
                                    <td class="text-right">$220,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Other Non-Financial Assets</th>
                                    <td class="text-right">$90,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Superannuation</th>
                                    <td class="text-right">$113,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Other Financial Assets</th>
                                    <td class="text-right">$215,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Financial Liabilities</th>
                                    <td class="text-right">-$58,000</td>
                                </tr>
                                <tr>
                                    <th class="text-left">Total</th>
                                    <td class="text-right">$1,170,000</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <small>Based on Australian Bureau of Statistics customised report, 2018.</small>
                </div>
                <br />
            </div>

            <br />
            {{> my_igtt }}
            <br />
            <div class="inner">
                <br />
                <h3>So, is an IGTT worth it?</h3>
                <p>In making your own mind up consider two perspectives. You cannot ignore the potential financial
                    consequences for yourself and your family, and they may be strongly positive or negative, but most
                    likely in-between.</p>
                <p>Self-interest is a powerful guide, but a fallible one at a whole-of-society level. Self-interests
                    conflict, and if picking between them consists of no more than giving the nod to the largest
                    number or the loudest voices I've little doubt the outcomes will be second rank, if that.</p>
                <p>It needs to be recognised that there are very significant transition issues in a policy change such
                    as this. Those approaching middle age and beyond would very likely be worse off under an IGTT, as
                    the reductions to income tax would only offset the loss of a small share of a small inheritance.
                    Fairness requires that groups that stand to be significantly and adversely affected should be able
                    to access transitional provisions. There are several options available; what course should be
                    adopted would require careful study and consideration.</p>
                <p>Quite often the best guide to good public policy is to determine which side of an argument has
                    the loudest voices and the narrowest range of supporters. That will be the group with the most
                    advantage to defend, and the strongest self interest to do so. Their advantage is most likely at
                    your expense.</p>
                <p>Step back. Consider what the national interest might be, and what role you could play in it. Do
                    we want a society for our children which encourages them to do their best, to 'have a go' and to
                    succeed according to their merits? Or do we want a society in which our talents and effort take
                    second place to our parents postcode and the largesse they can drop in our lap? I know which I
                    want, for my children and for all others.</p>
                <p>Nereus</p>
            </div>
        </div>
    </div>
</div>
</div>