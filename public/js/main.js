$(function() {
  // Submit income tax form
  $('.designing-an-igtt').submit(function(event) {
    event.preventDefault();

    var data = $(this).serialize();
    var $loader = $(event.target).find('.loading');

    // Retrive model name from data attribute
    var modelName = $(this).data('modelName');
    // End if not model is specified
    if (!modelName) return false;

    var $results = $(event.target).siblings('.results.' + modelName);

    // Show the loading animation
    $loader.show();
    // Make the ajax request
    $.ajax({
      url: '/calculate/' + modelName,
      method: 'post',
      data: data,
    }).done(function(table) {
      // Replace the results table with updated data
      $results.html(table);
      // Hide the loading animation
      $loader.hide();
    });
    return false;
  });

  // Reset single form
  $('.reset-form').click(function(event) {
    var model = $('.designing-an-igtt').data('modelName');
    $(event.target)
    .closest('form.designing-an-igtt')
    .trigger('reset');
    $(event.target)
    .siblings('div')
    .find('.loading')
    .hide();
    $(event.target)
    .closest('form.designing-an-igtt')
    .siblings('div.results')
    .find('div.row')
    .remove();
    $.post('/user/reset-form/' + model, function(err, res) {
      if (err) {
        console.log(err);
      }
    });
  });

  var current = location.pathname;
  $('#navbar ul li a').each(function() {
    var $this = $(this);
    // if the current path is like this link, make it active
    if ($this.attr('href').indexOf(current) !== -1) {
      $this.addClass('active');
    }
  });

  // Reverse the New Supporters table order
    var tbody = $('.petition-table tbody');
    tbody.html($('tr',tbody).get().reverse());


  // Counter on New Supporters
    var count = $('table.petition-table tbody').children('tr').length;
    $('#counter').html(count);

});
