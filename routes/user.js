var express = require('express');
var router = express.Router();
var passport = require('passport');
var crypto = require('crypto');
const config = require('platformsh-config').config();
const sgMail = require('@sendgrid/mail');

const emailFrom = 'noreply@amuchbettertax.org';

if (config.isValidPlatform()) {
  var sgKey = config.variable('SENDGRID_API_KEY');
  sgMail.setApiKey(sgKey);
}

/**
 * Logging In
 */
router.post('/login', function(req, res, next) {
  passport.authenticate('local', { session: true }, function(error, user, info) {
    if (info.message === 'Account not activated.') {
      return res.render('login', { title: 'Login', notVerified: true });
    } else if (error || !user) {
      return res.render('login', { title: 'Login', noUser: true });
    } else {
      req.logIn(user, function(err) {
        if (err) {
          return next(err);
        }
        if (req.session.prevPage) {
          return res.redirect('/' + req.session.prevPage);
        } else {
          return res.redirect('/new-supporters');
        }
      });
    }
  })(req, res);
});

/**
 * Register
 */
router.post('/register', function(req, res) {
  req.db.collection('users').findOne({ email: req.body.user }, function(err, user) {
    if (user) {
      res.render('register', { title: 'register', accountExists: true });
    } else {
      hashPassword(req.body, function(newUser) {
        if (req.body.occupation == '') {
          var occupation = 'Not Supplied';
        } else {
          var occupation = req.body.occupation;
        }
        if (req.body.postcode == '') {
          var postcode = 'Not Supplied';
        } else {
          var postcode = req.body.postcode;
        }
        crypto.randomBytes(20, function(err, buf) {
          const verifyToken = buf.toString('hex');
          const activateTokenExpires = Date.now() + 86400000; // 24 hours
          const permalink = req.body.firstName + req.body.lastName;
          req.db.collection('users').insert(
            {
              title: req.body.title,
              firstName: req.body.firstName,
              lastName: req.body.lastName,
              email: newUser.user,
              password: newUser.password,
              postcode: postcode,
              occupation: occupation,
              verificationToken: verifyToken,
              activated: false,
              permalink: permalink.toLowerCase(),
              activateTokenExpires: activateTokenExpires,
            },
            function(err, record) {
              if (err) {
                res.status(401).send(err);
              }
              console.log(
                'http://' +
                  req.headers.host +
                  '/user/verify/' +
                  permalink.toLowerCase() +
                  '/' +
                  verifyToken
              );
              sgMail.send(
                {
                  to: newUser.user,
                  from: 'A Much Better Tax <' + emailFrom + '>',
                  subject: 'A Much Better Tax Account Verification',
                  text:
                    'You are receiving this because you have signed up for an A Much Better Tax account.\n\n' +
                    'Please click on the following link, or paste this into your browser to complete the process and activate your account:\n\n' +
                    'http://' +
                    req.headers.host +
                    '/user/verify/' +
                    permalink.toLowerCase() +
                    '/' +
                    verifyToken +
                    '\n\n' +
                    'If you did not request this, please ignore this email.\n',
                },
                function(err, message) {
                  res.redirect('login?accountCreated=true');
                }
              );
            }
          );
        });
      });
    }
  });
});

/*
 * Verify Email
 */

router.get('/verify/:permalink/:token', function(req, res) {
  const verifyPermalink = req.params.permalink;
  const verifyToken = req.params.token;

  req.db
    .collection('users')
    .findOne({ permalink: verifyPermalink, verificationToken: verifyToken }, function(err, user) {
      if (!user) {
        console.log(err);
        res.redirect('/user/register?userNotFound=true');
      } else if (user.verificationToken === verifyToken && user.activateTokenExpires > Date.now()) {
        req.db.collection('users').update(
          { email: user.email },
          {
            $set: {
              activated: true,
            },
            $unset: {
              verificationToken: null,
              permalink: null,
              activateTokenExpires: null,
            },
          },
          function(err, message) {
            res.redirect('/user/login?accountVerified=true');
          }
        );
      } else if (user.verificationToken === verifyToken && user.activateTokenExpires < Date.now()) {
        req.db.collection('users').remove({ email: user.email });
        res.redirect('/user/register?expiredToken=true');
      } else {
        req.db.collection('users').remove({ email: user.email });
        res.redirect('/user/register?invalidToken=true');
      }
    });
});

/**
 * Forgot Password
 */
router.post('/forgot', function(req, res) {
  req.db.collection('users').findOne({ email: req.body.email }, function(err, user) {
    if (user) {
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        var resetPasswordExpires = Date.now() + 3600000; // 1 hour

        req.db
          .collection('users')
          .update(
            { email: req.body.email },
            { $set: { resetPasswordToken: token, resetPasswordExpires: resetPasswordExpires } },
            function(err, record) {
              sgMail.send(
                {
                  to: user.email,
                  from: 'A Much Better Tax <' + emailFrom + '>',
                  subject: 'A Much Better Tax Password Reset',
                  text:
                    'You are receiving this because you (or someone else) have requested the reset of the password for your A Much Better Tax account.\n\n' +
                    'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                    'https://' +
                    req.headers.host +
                    '/user/reset/' +
                    token +
                    '\n\n' +
                    'If you did not request this, please ignore this email and your password will remain unchanged.\n',
                },
                function(err, message) {
                  res.render('forgot', { title: 'Forgot', sent: true });
                }
              );
            }
          );
      });
    } else {
      res.render('forgot', { title: 'Forgot', noEmail: true });
    }
  });
});

/**
 * Reset Password
 */
router.get('/reset/:token', function(req, res, next) {
  req.db
    .collection('users')
    .findOne(
      { resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } },
      function(err, user) {
        if (user) {
          res.render('reset', { title: 'Reset' });
        } else {
          res.redirect('/user/forgot');
        }
      }
    );
});

router.post('/reset/:token', function(req, res, next) {
  req.db
    .collection('users')
    .findOne(
      { resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } },
      function(err, user) {
        if (user) {
          hashPassword(req.body, function(user) {
            req.db.collection('users').update(
              { resetPasswordToken: req.params.token },
              {
                $set: {
                  password: user.password,
                  resetPasswordToken: null,
                  resetPasswordExpires: null,
                },
              },
              function(err, response) {
                if (err || !response) {
                  res.status(401).send(err);
                } else {
                  res.render('reset', { title: 'Reset', reset: true });
                }
              }
            );
          });
        } else {
          res.redirect('/user/forgot');
        }
      }
    );
});

/**
 * Reset all form data
 */
router.post('/reset-all', function(req, res, next) {
  req.db
    .collection('data')
    .update({ email: req.user.email }, { $set: { data: {} } }, function(error, user) {
      if (error) {
        return error;
      } else {
        return user;
      }
    });
});

/**
 * Reset single form data
 */
router.post('/reset-form/:model', function(req, res, next) {
  var query = {};
  query['data.' + req.params.model] = {};
  req.db
    .collection('data')
    .update({ email: req.user.email }, { $set: query }, function(error, user) {
      if (error) {
        return error;
      } else {
        return user;
      }
    });
});

/**
 * Login form
 */
router.get('/login', function(req, res, next) {
  req.session.prevPage = req.query.ref;
  if (req.user) {
    res.redirect('/' + req.session.prevPage);
  } else {
    res.render('login', {
      title: 'Login',
      accountVerified: req.query.accountVerified,
      accountCreated: req.query.accountCreated,
    });
  }
});

/**
 * Register form
 */
router.get('/register', function(req, res, next) {
  if (req.user) {
    res.redirect('/');
  } else {
    res.render('register', {
      title: 'Register',
      invalidToken: req.query.invalidToken,
      userNotFound: req.query.userNotFound,
      expiredToken: req.query.expiredToken,
    });
  }
});

/**
 * Forgot password form
 */
router.get('/forgot', function(req, res, next) {
  if (req.user) {
    res.redirect('/');
  } else {
    res.render('forgot', { title: 'Forgot Password' });
  }
});

/**
 * Reset password form
 */
router.get('/reset', function(req, res, next) {
  if (req.user) {
    res.redirect('/');
  } else {
    res.render('reset', { title: 'Reset Password' });
  }
});

/**
 * Validate a user token
 */
// router.get('/validate', passwordless.acceptToken({ successRedirect: '/' }),
//   // If the token did not validate the redirect to the login page
//   function(req, res, next) {
//     res.redirect('/user/login');
//   }
// );

/**
 * Logout the current user
 */
router.get('/logout', function(req, res, next) {
  var page = '/' + req.query.ref;
  req.logout();
  res.redirect(page);
});

//Sign the petition

router.post('/sign', function(req, res) {
  req.db.collection('users').update(
    { email: req.user.email },
    {
      $set: { supporter: true, dateSigned: new Date() },
    },
    function(err, user) {
      res.redirect('back');
    }
  );
});

//Unsign the petition

router.post('/unsign', function(req, res) {
  req.db.collection('users').update(
    { email: req.user.email },
    {
      $set: { supporter: false },
    },
    function(err, user) {
      res.redirect('back');
    }
  );
});

module.exports = router;
